﻿using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
