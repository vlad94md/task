﻿using System;

namespace ApplicationCore.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }
        public Customer Owner { get; set; }
    }
}