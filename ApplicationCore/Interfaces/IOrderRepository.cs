﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces
{
    public interface IOrderRepository
    {
        Task<List<Order>> GetOrdersForCustomer(int customerId);
        Task<Order> CreateAsync(Order order);
    }
}
