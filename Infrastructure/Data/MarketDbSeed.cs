﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class MarketDbSeed
    {
        public static void Seed(MarketDbContext context)
        {
            if (!context.Customers.Any())
            {
                context.Customers.AddRange(
                    CustomersItems());

                context.SaveChanges();
            }

            if (!context.Orders.Any())
            {
                context.Orders.AddRange(
                    OrdersItems(context));

                context.SaveChanges();
            }
        }

        public static List<Customer> CustomersItems()
        {
            var result = new List<Customer>
            {
                new Customer()
                {
                    Email = "test@gmail.com",
                    Name = "Vladislav Guleaev",
                    Password = "123456"
                },
                new Customer()
                {
                    Email = "doe@gmail.com",
                    Name = "John Doe",
                    Password = "123456"
                },
                new Customer()
                {
                    Email = "jon@gmail.com",
                    Name = "Jon Snow",
                    Password = "123456"
                }
            };

            return result;
        }

        public static List<Order> OrdersItems(MarketDbContext context)
        {
            Random rnd = new Random();

            var result = new List<Order>
            {
                new Order()
                {
                    CreatedDate = DateTime.Now,
                    Price = rnd.Next(1, 600),
                    Owner = context.Customers.Skip(0).ToList().First()
                },
                new Order()
                {
                    CreatedDate = DateTime.Now,
                    Price = rnd.Next(1, 600),
                    Owner = context.Customers.Skip(0).ToList().First()
                },
                new Order()
                {
                    CreatedDate = DateTime.Now,
                    Price = rnd.Next(1, 600),
                    Owner = context.Customers.Skip(1).ToList().First()
                },
                new Order()
                {
                    CreatedDate = DateTime.Now,
                    Price = rnd.Next(1, 600),
                    Owner = context.Customers.Skip(2).ToList().First()
                }
            };

            return result;
        }
    }
}
