﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly MarketDbContext _dbContext;

        public OrderRepository(MarketDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<List<Order>> GetOrdersForCustomer(int customerId)
        {
            return _dbContext.Orders
                .Where(x => x.Owner.Id == customerId)
                .ToListAsync();
        }

        public async Task<Order> CreateAsync(Order order)
        {
            _dbContext.Orders.Add(order);
            await _dbContext.SaveChangesAsync();

            return order;
        }
    }
}
