﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using Infrastructure.Data;
using Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Microsoft.Extensions.Configuration;

namespace IntegrationTests.Repositories
{
    public class CustomerRepositoryTests: IDisposable
    {
        private readonly MarketDbContext _context;
        private CustomerRepository _customerRepository;

        public CustomerRepositoryTests()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            var configuration = builder.Build();

            var dbOptions = new DbContextOptionsBuilder<MarketDbContext>()
                .UseSqlServer(configuration["ConnectionStrings:TestConnection"])
                .Options;
            _context = new MarketDbContext(dbOptions);
            _customerRepository = new CustomerRepository(_context);
        }

        [Fact]
        public async Task GetById()
        {
            //Arrange
            var customerToInsert = new Customer()
            {
                Name = "test"
            };

            _context.Customers.Add(customerToInsert);
            await _context.SaveChangesAsync();

            var id = customerToInsert.Id;

            //Act
            var result = await _customerRepository.GetById(id);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(customerToInsert.Name, result.Name);
        }

        [Fact]
        public async Task GetByEmail()
        {
            //Arrange
            var customerToInsert = new Customer()
            {
                Email = "test@mail.com"
            };

            _context.Customers.Add(customerToInsert);
            await _context.SaveChangesAsync();

            var email = customerToInsert.Email;

            //Act
            var result = await _customerRepository.GetByEmail(email);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(customerToInsert.Email, result.Email);
        }

        [Fact]
        public async Task GetAll()
        {
            //Arrange
            var customersToInsert = new List<Customer>() {new Customer(), new Customer(), new Customer()};
            
            _context.Customers.AddRange(customersToInsert);
            await _context.SaveChangesAsync();

            //Act
            var result = await _customerRepository.GetAll();

            //Assert
            Assert.Equal(customersToInsert.Count, result.Count);
        }

        [Fact]
        public async Task CreateAsync()
        {
            //Arrange
            var customerToInsert = new Customer()
            {
                Email = "test@mail.com",
                Name = "test",
                Password = "123456"
            };

            //Act
            var result = await _customerRepository.CreateAsync(customerToInsert);

            //Assert
            Assert.Equal(customerToInsert.Email, result.Email);
            Assert.Equal(customerToInsert.Name, result.Name);
            Assert.NotEqual(0, result.Id);
        }

        public void Dispose()
        {
            _context.Database.ExecuteSqlCommand("delete from customers");
            _context.Database.ExecuteSqlCommand("delete from orders");
        }
    }
}
