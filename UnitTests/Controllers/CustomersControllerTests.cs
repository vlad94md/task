﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Web;
using Web.Constants;
using Web.Controllers;
using Web.Models;
using Web.Models.Dto;
using Xunit;

namespace UnitTests.Controllers
{
    public class CustomersControllerTests
    {
        private Mock<ICustomerRepository> customerRepositoryMock;
        private Mock<IOrderRepository> orderRepositoryMock;
        private CustomersController controller;

        static CustomersControllerTests()
        {
            AutoMapper.Mapper.Initialize(AutoMapperConfig.Config);
        }

        public CustomersControllerTests()
        {
            customerRepositoryMock = new Mock<ICustomerRepository>();
            orderRepositoryMock = new Mock<IOrderRepository>();
            controller = new CustomersController(customerRepositoryMock.Object, orderRepositoryMock.Object);
        }

        [Fact]
        public async Task GetAll_WhenCalled_ReturnCustomers()
        {
            //Arrange
            var customers = new List<Customer>() {new Customer(), new Customer(), new Customer()};
            customerRepositoryMock.Setup(x => x.GetAll()).ReturnsAsync(customers);

            //Act
            var result = await controller.GetAll();

            //Assert
            var httpResponse = Assert.IsType<OkObjectResult>(result);
            var customersList = Assert.IsType<List<CustomerDto>>(httpResponse.Value);
            Assert.Equal(3, customersList.Count);
        }

        [Fact]
        public async Task GetById_WithValidId_ReturnCustomerWithOrders()
        {
            //Arrange
            var id = 1;
            var customer = new Customer() {Id = 1, Name = "TestUser"};
            customerRepositoryMock.Setup(x => x.GetById(It.Is<int>(s => s == id))).ReturnsAsync(customer);

            var orders = new List<Order>() {new Order(), new Order(), new Order()};
            orderRepositoryMock.Setup(x => x.GetOrdersForCustomer(It.Is<int>(s => s == id))).ReturnsAsync(orders);

            //Act
            var result = await controller.GetById(id);

            //Assert
            var httpResponse = Assert.IsType<OkObjectResult>(result);
            var customerDto = Assert.IsType<CustomerDto>(httpResponse.Value);
            Assert.Equal(id, customerDto.Id);
            Assert.Equal("TestUser", customerDto.Name);
            Assert.Equal(orders.Count, customerDto.Orders.Count);
        }

        [Fact]
        public async Task GetById_WithInvalidId_ReturnNotFound()
        {
            //Arrange
            var id = 1;

            customerRepositoryMock.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync((Customer)(null));

            //Act
            var result = await controller.GetById(id);

            //Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task CreateCustomer_EmailIsUnique_ReturnOk()
        {
            //Arrange
            var createModel = new CreateCustomerModel()
            {
                Name = "Test",
                Email = "test@email.com",
                Password = "123456789"
            };
            customerRepositoryMock.Setup(x => x.GetByEmail(It.IsAny<string>())).ReturnsAsync((Customer)(null));

            //Act
            var result = await controller.CreateCustomer(createModel);

            //Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task CreateCustomer_EmailAlreadyExists_ReturnCustomerAlreadyExistsErrorMessage()
        {
            //Arrange
            var createModel = new CreateCustomerModel()
            {
                Name = "Test",
                Email = "test@email.com",
                Password = "123456789"
            };
            customerRepositoryMock.Setup(x => x.GetByEmail(It.IsAny<string>())).ReturnsAsync(new Customer());

            //Act
            var result = await controller.CreateCustomer(createModel);

            //Assert
            var response = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(ValidationMessages.EmailExists, response.Value);
        }
    }
}
