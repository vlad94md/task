﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Web;
using Web.Constants;
using Web.Controllers;
using Web.Models;
using Web.Models.Dto;
using Xunit;

namespace UnitTests.Controllers
{
    public class OrdersControllerTests
    {
        private Mock<ICustomerRepository> customerRepositoryMock;
        private Mock<IOrderRepository> orderRepositoryMock;
        private OrdersController controller;

        static OrdersControllerTests()
        {
            AutoMapper.Mapper.Initialize(AutoMapperConfig.Config);
        }

        public OrdersControllerTests()
        {
            customerRepositoryMock = new Mock<ICustomerRepository>();
            orderRepositoryMock = new Mock<IOrderRepository>();
            controller = new OrdersController(customerRepositoryMock.Object, orderRepositoryMock.Object);
        }

        [Fact]
        public async Task CreateOrder_PriceIsNegative_ReturnPriceIsNegativeErrorMessage()
        {
            //Arrange
            int id = 1;
            var createModel = new CreateOrderModel()
            {
                Price = -1
            };

            //Act
            var result = await controller.CreateOrder(id, createModel);

            //Assert
            var response = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(ValidationMessages.PriceIsNegativeOrZero, response.Value);
        }

        [Fact]
        public async Task CreateOrder_InvalidCustomerId_ReturnCustomerNotFoundErrorMessage()
        {
            //Arrange
            int id = 1;
            var createModel = new CreateOrderModel()
            {
                Price = 100
            };
            customerRepositoryMock.Setup(x => x.GetById(It.Is<int>(i => i == id)))
                .ReturnsAsync((Customer)(null));

            //Act
            var result = await controller.CreateOrder(id, createModel);

            //Assert
            var response = Assert.IsType<NotFoundObjectResult>(result);
            Assert.Equal(ValidationMessages.CustomerNotFound, response.Value);
        }

        [Fact]
        public async Task CreateCustomer_WhenCalled_ReturnOk()
        {
            //Arrange
            int id = 1;
            var createModel = new CreateOrderModel()
            {
                Price = 100
            };
            customerRepositoryMock.Setup(x => x.GetById(It.Is<int>(i => i == id)))
                .ReturnsAsync(new Customer() { Id = id});

            //Act
            var result = await controller.CreateOrder(id, createModel);

            //Assert
            Assert.IsType<OkObjectResult>(result);
        }
    }
}
