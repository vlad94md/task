﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Constants
{
    public class ValidationMessages
    {
        public const string EmailExists = "Customer with such email already exists.";
        public const string PriceIsNegativeOrZero = "Price cannot be negative or zero.";
        public const string CustomerNotFound = "Customer not found.";
    }
}
