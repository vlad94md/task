﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Web.Constants;
using Web.Models;
using Web.Models.Dto;

namespace Web.Controllers
{
    [Route("v1/customers")]
    public class CustomersController : Controller
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;

        public CustomersController(ICustomerRepository customerRepository, IOrderRepository orderRepository)
        {
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAll()
        {
            var customers = await _customerRepository.GetAll();

            var customerDtos = Mapper.Map<IEnumerable<CustomerDto>>(customers);

            return Ok(customerDtos);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var customer = await _customerRepository.GetById(id);

            if (customer == null)
                return NotFound();

            var ordersForCustomer = await _orderRepository.GetOrdersForCustomer(id);

            var customerDto = Mapper.Map<CustomerDto>(customer);
            var orderDtos = Mapper.Map<IEnumerable<OrderDto>>(ordersForCustomer);
            customerDto.Orders = orderDtos.ToList();

            return Ok(customerDto);
        }

        [HttpPost("")]
        public async Task<IActionResult> CreateCustomer([FromForm] CreateCustomerModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (await _customerRepository.GetByEmail(model.Email) != null)
                return BadRequest(ValidationMessages.EmailExists);

            var customer = Mapper.Map<Customer>(model);

            await _customerRepository.CreateAsync(customer);
            var customerDto = Mapper.Map<CustomerDto>(customer);

            return Ok(customerDto);
        }
    }
}