﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Web.Constants;
using Web.Models;
using Web.Models.Dto;

namespace Web.Controllers
{
    [Route("v1/customers/{id}/orders")]
    public class OrdersController : Controller
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;

        public OrdersController(ICustomerRepository customerRepository, IOrderRepository orderRepository)
        {
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
        }

        [HttpPost("")]
        [Authorize]
        public async Task<IActionResult> CreateOrder(int id, [FromForm] CreateOrderModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (model.Price <= 0)
                return BadRequest(ValidationMessages.PriceIsNegativeOrZero);

            var customer = await _customerRepository.GetById(id);

            if (customer == null)
                return NotFound(ValidationMessages.CustomerNotFound);

            var order = new Order()
            {
                CreatedDate = DateTime.Now,
                Price = model.Price,
                Owner = customer
            };

            await _orderRepository.CreateAsync(order);
            var orderDto = Mapper.Map<OrderDto>(order);

            return Ok(orderDto);
        }
    }
}