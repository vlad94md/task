﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using Web.Models;
using Web.Models.Dto;

namespace Web
{
    public class AutoMapperConfig
    {
        public static void Config(AutoMapper.IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Customer, CustomerDto>();
            cfg.CreateMap<Order, OrderDto>();
            cfg.CreateMap<CreateCustomerModel, Customer>();
        }
    }
}
